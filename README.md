# app

This is a technical test app for this challenge you will need to fix the following issues:
The movies-app it the react front end application
The api folder contains a small express api

1. Several images from the app are not loading properly
2. Fix the TODO list in order for the react application
3. Add the required unit testing for your fixes
4. Each solution needs to have a unique commit
5. Move to the backend challenge and complete the TODO list
6. Add to the front end a new feature to sell movies to another studio
7. (Optional) fix any vulnerabilities you find
