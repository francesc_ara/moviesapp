const domain = 'http://localhost:3000';

const getStudios = () => {
  return fetch(`${domain}/studios`).then(response => response.json());
}

const getMovies = () => {
  return fetch(`${domain}/movies`).then(response => response.json());
}

export default {
  getStudios,
  getMovies
}