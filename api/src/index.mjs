import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser'
import {getAllMoviesFromStudios, sellMovieToStudio} from '../src/helpers.mjs'
import {sony, warner, disney, movieAge} from '../constants/studio_constants.mjs'

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/studios', function (req, res) {
  let disneyTemp = {...disney}
  delete disneyTemp.movies
  let warnerTemp = {...warner}
  delete warnerTemp.movies
  let sonyTemp = {...sony}
  delete sonyTemp.movies
  res.json([
    disneyTemp,
    warnerTemp,
    sonyTemp
  ])
});

app.get('/services', function (req, res) {
  try {
    res.json(getAllMoviesFromStudios([disney, warner, sony]))
  } catch (e) {
    res.status(500)
  }
});

app.get('/movieAge', function (req, res) {
  res.json(movieAge)
});

//TODO: 1 add the capability to sell the movie rights to another studio
app.post('/transfer', function (req, res) {
  const body = req.body;
  const { movie, sellingStudio, buyingStudio } = body;

  try {
    res.json(sellMovieToStudio(movie, sellingStudio, buyingStudio))
  } catch (e) {
    if (e.message === 'Movie price exceeds Studio budget') {
      res.status(403);
    }
    res.status(500);
  }
});

// TODO: 2 Add logging capabilities into the services-app
app.use((req, res) => {
  console.log('Method:', req.method)
  console.log('Path:  ', req.path)
  console.log('Body:  ', req.body)
  console.log('---')
})

app.listen(3000)
