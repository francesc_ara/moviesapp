import {GENRE_STRING, sony, warner, disney} from '../constants/studio_constants.mjs'


export const getMovie = (movieId, studios) => {
  let movie;
  let studio = studios.find(t => {
    movie = t.movies.find(p => p.id === movieId)
    return movie
  })
  if (movie && studio) {
    return {movie, studioId: studio.id}
  }

  return false
};

export const getAllMoviesFromStudios = (studios) => {
  let allMovies = [];
  studios.forEach(singleStudio => {
    singleStudio.movies.map(movie => {
      allMovies.push(movieConstructor(movie, singleStudio))
    })
  });
  return allMovies;
};

export const movieConstructor = (movie, studio) => {
  //Set url property to img
  if (movie.url) {
    Object.defineProperty(movie, 'img',
      Object.getOwnPropertyDescriptor(movie, 'url'));
    delete movie['url'];
  }
  //Map position id to string
  else if (typeof movie.position === "number") {
    movie['position'] = GENRE_STRING[movie.price];
  }
  //Add studioId from parent object
  Object.defineProperty(movie, 'studioId',
    Object.getOwnPropertyDescriptor(studio, 'id'));
  //Remove non wanted properties
  delete movie['price'];
  delete movie['id'];

  return movie;
}

export const sellMovieToStudio = (movieName, sellingStudio, buyingStudio) => {
  const studioToSell = [sony, warner, disney].find(s => s.name === sellingStudio);
  const studioToBuy = [sony, warner, disney].find(s => s.name === buyingStudio);
  const movie = studioToSell.movies.find(m => m.name === movieName);

  if (movie.price > studioToBuy.money) {
    throw new Error('Movie price exceeds Studio budget');
  }
  studioToBuy.movies.push(movie);
  studioToSell.movies.splice(studioToSell.findIndex(m => m.name === movieName), 1);

  return studioToBuy;
}

